# Django Weather App

## Description

This Django project fetches and displays real-time and forecast weather information for various cities around the globe. Utilizing the OpenWeatherMap API, it offers users up-to-date weather forecasts including temperature, humidity, pressure, and visibility. The project aims to provide an easy-to-use web interface for quick weather checks.

## URLs
http://127.0.0.1:8000/    ## including current data

http://127.0.0.1:8000/hour/    ## including 24 hours later based on  Tehran

http://127.0.0.1:8000/day/    ## including 5 days later 


## How to Use

To set up and run this project locally, follow these steps:

1. **Clone the repository**:

```bash
git clone https://gitlab.com/nzbabaeii/weather.git
cd weather
python3 -m venv venv
# Activate the virtual environment
# On Windows
venv\Scripts\activate
# On Unix or MacOS
source venv/bin/activate
pip install -r requirements.txt
# Databases Migrations:
python manage.py makemigrations
python manage.py migrate

#API_KEY :
export OPENWEATHERMAP_API_KEY= <your api key>
#Run
python manage.py runserver

# After running the server, visit http://localhost:8000 in your web browser to view the app.