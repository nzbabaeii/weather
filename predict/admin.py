from django.contrib import admin 
from .models import *

admin.site.register(City)
admin.site.register(Weather24)
admin.site.register(WeatherCurrent)
admin.site.register(Weather5)
