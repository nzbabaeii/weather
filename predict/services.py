import requests
import os
import csv
api_key = os.getenv('OPENWEATHERMAP_API_KEY')  #api key by export command


def get_city_names_from_csv(csv_file_path):
    """
    Reads city names from a CSV file. Assumes each row contains one city name in the first column.

    :param csv_file_path: Path to the CSV file containing city names.
    :return: A list of city names.
    """
    city_names = []
    with open(csv_file_path, newline='') as csvfile:
        reader = csv.reader(csvfile)
        for row in reader:
            city_names.append(row[0])  # Assuming each row contains one city name in the first column.
    return city_names


def fetch_city_data(city_name):
    url = f"http://api.openweathermap.org/data/2.5/weather?q={city_name}&appid={api_key}&units=metric"
    response = requests.get(url)
    return response.json()

def fetch_weather_data(lat, lon):
    url = f"http://api.openweathermap.org/data/2.5/forecast?lat={lat}&lon={lon}&appid={api_key}&units=metric"
    response = requests.get(url)
    return response.json()