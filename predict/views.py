from django.shortcuts import render
from django.utils import timezone
from datetime import timedelta
from .models import *
from .services import *


def next_24hours_later(request):
    csv_file_path = os.path.join(os.path.dirname(__file__), 'city_names.csv')
    
    # Use the function to get city names
    city_names = get_city_names_from_csv(csv_file_path)
    for city_name in city_names:
        city_data = fetch_city_data(city_name)
        if city_data:
            # Update or create a city record for the database
            city, created = City.objects.update_or_create(
                name__iexact=city_name,
                defaults={
                    'name': city_name,
                    'city_id': city_data['id'],
                    'country': city_data['sys']['country'],
                    'latitude': city_data['coord']['lat'],
                    'longitude': city_data['coord']['lon'],
                
                }
            )

            lat, lon = city.latitude, city.longitude
            weather_data = fetch_weather_data(lat, lon)
            
            if 'list' in weather_data and len(weather_data['list']) >= 8:
                forecast_24h = weather_data['list'][7]  # Assuming 8th entry is 24 hours later
                
                # Update or create a Weather24 record for the city on database
                Weather24.objects.update_or_create(
                    city=city,
                    defaults={
                        'temperature': forecast_24h['main']['temp'],
                        'pressure': forecast_24h['main']['pressure'],
                        'humidity': forecast_24h['main']['humidity'],
                        'visibility': forecast_24h.get('visibility', 0),
                        'description': forecast_24h['weather'][0]['description'],
                        'icon': forecast_24h['weather'][0]['icon'],
                        'timestamp': timezone.now() + timedelta(days=1),
                    }
                )

    weather_reports = Weather24.objects.all()
    return render(request, 'predict/hourly.html', {'weather_reports': weather_reports})


def current(request):
    csv_file_path = os.path.join(os.path.dirname(__file__), 'city_names.csv')
    city_names = get_city_names_from_csv(csv_file_path)

    for city_name in city_names:
        weather_data = fetch_city_data(city_name)
        if 'id' in weather_data:
            city, created = City.objects.update_or_create(
                name__iexact=city_name,
                defaults={
                    'name': city_name,  
                    'city_id': weather_data['id'],
                    'country': weather_data['sys']['country'],
                     
                }
            )
            
            
            WeatherCurrent.objects.update_or_create(
                city=city,
                defaults={
                    'temperature': weather_data['main']['temp'],
                    'pressure': weather_data['main']['pressure'],
                    'humidity': weather_data['main']['humidity'],
                    'visibility': weather_data.get('visibility', 0),  # Handling potentially missing data
                    'description': weather_data['weather'][0]['description'],
                    'icon': weather_data['weather'][0]['icon'],
                    'timestamp': timezone.now(),
                }
            )
        else:
            print(f"Error fetching weather data for {city_name}")

    weather_reports = WeatherCurrent.objects.all()  # Fetch all weather reports to display
    return render(request, 'predict/current.html', {'weather_reports': weather_reports})


def next_5days_later(request):
    csv_file_path = os.path.join(os.path.dirname(__file__), 'city_names.csv')
    city_names = get_city_names_from_csv(csv_file_path)
    
    for city_name in city_names:
        city_data = fetch_city_data(city_name)
        if city_data:
            city, created = City.objects.update_or_create(
                name__iexact=city_name,
                defaults={
                    'name': city_name,
                    'city_id': city_data['id'],
                    'country': city_data['sys']['country'],
                    'latitude': city_data['coord']['lat'],
                    'longitude': city_data['coord']['lon'],
                }
            )

            lat, lon = city.latitude, city.longitude
            weather_data = fetch_weather_data(lat, lon)
            
            # Adjust to find forecast approximately 5 days later
            if 'list' in weather_data and len(weather_data['list']) > 39:  # Checking for sufficient data
                forecast_5days = weather_data['list'][39]  # Assuming the 40th entry is 5 days later
                
                Weather5.objects.update_or_create(
                    city=city,
                    defaults={
                        'temperature': forecast_5days['main']['temp'],
                        'pressure': forecast_5days['main']['pressure'],
                        'humidity': forecast_5days['main']['humidity'],
                        'visibility': forecast_5days.get('visibility', 0),
                        'description': forecast_5days['weather'][0]['description'],
                        'icon': forecast_5days['weather'][0]['icon'],
                        'timestamp': timezone.now() + timedelta(days=5),
                    }
                )

    weather_reports = Weather5.objects.all()
    return render(request, 'predict/daily.html', {'weather_reports': weather_reports})
