from django.db import models


class City(models.Model):
    city_id= models.IntegerField()
    latitude=models.IntegerField(blank=True , null=True)
    longitude=models.IntegerField(blank=True , null=True)
    name = models.CharField(max_length=200)
    country=models.CharField(max_length=200)
  

    class Meta:
        verbose_name = "City"
        verbose_name_plural = "Cities"

    def __str__(self):
        return self.name


class Weather24(models.Model):
    city=models.ForeignKey(City, on_delete=models.CASCADE)
    temperature=models.DecimalField(max_digits=12,decimal_places=2)
    pressure=models.IntegerField()
    humidity=models.IntegerField()
    visibility=models.IntegerField()
    description=models.TextField()
    icon=models.URLField()
    timestamp=models.DateTimeField(auto_now_add=True)


    def __str__(self):
        
        return self.city.name
    


class WeatherCurrent(models.Model):
    city=models.ForeignKey(City, on_delete=models.CASCADE)
    temperature=models.DecimalField(max_digits=12,decimal_places=2)
    pressure=models.IntegerField()
    humidity=models.IntegerField()
    visibility=models.IntegerField()
    description=models.TextField()
    icon=models.URLField()
    timestamp=models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.city.name


class Weather5(models.Model):
    city=models.ForeignKey(City, on_delete=models.CASCADE)
    temperature=models.DecimalField(max_digits=12,decimal_places=2)
    pressure=models.IntegerField()
    humidity=models.IntegerField()
    visibility=models.IntegerField()
    description=models.TextField()
    icon=models.URLField()
    timestamp=models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.city.name