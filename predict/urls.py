from django.urls import path , include
from . import views



urlpatterns = [
    path('',views.current),
    path('hour/', views.next_24hours_later),
    path('day/', views.next_5days_later),

]